# temperature.py
# Parses incoming micro:bit serial data and pushes to an InfluxDB instance.
# Basic authentication also implemented to ensure false data isn't received.
# CardyHoney, 2021 (Jacob Passam)

import os, serial, io, datetime, json
from influxdb import InfluxDBClient
from time import sleep

DATABASE_CREDENTIALS_FILE="database.json"
AUTH_KEY_FILE = "auth_key.dat"
COUNTRY_FILE = "country.dat"

# Read authentication key from the file, or create it with a default one.
def get_auth_key():
    try:
        f = open(AUTH_KEY_FILE)
        return f.read().strip("\n")
    except OSError:
        # File not exists, write default.
        
        with open(AUTH_KEY_FILE, 'w') as writable:
            writable.write('YourAuthenticationKey')
    else:
        f.close()
        
        return 'YourAuthenticationKey'

def get_country():
    try:
        f = open(COUNTRY_FILE)
        return f.read().strip("\n")
    except OSError:
        # File not exists, write default.
        
        with open(COUNTRY_FILE, 'w') as writable:
            writable.write('Nepal')
    else:
        f.close()
        
        return 'Nepal'


# Wait for a micro:bit to be connected and return its /dev/ node address.
def get_microbit_node():
    print("Waiting for micro:bit node...")
    while True:
        try:
            node = "/dev/" + next(x for x in os.listdir("/dev") if x.startswith("ttyACM"))
            print("Found connected micro:bit!")
            sleep(2)
            return node
        except Exception as e:
            if isinstance(e, KeyboardInterrupt):
                raise
            pass

def get_credentials():
    with open(DATABASE_CREDENTIALS_FILE) as f:
        return json.load(f)

COUNTRY = get_country()

def send_data_to_influx(name, temp):
    # Provide a JSON body containing the data.
    body = {
        "measurement": "hive_condition",
        "tags": {
            "location": name,
            "partner": COUNTRY
            },
        "fields": {
            "temp": temp
            }
        }

    # Write the data to InfluxDB.
    client.write_points([body])

# Your authentication key: preferably a random string, up to 10 characters.
AUTH_KEY = get_auth_key()

mb_node = get_microbit_node()
# Create a serial reader.
ser = serial.Serial(mb_node, 115200, timeout=0.1)

# Decode the micro:bit 'device ID' into a location.
ID_LOCATION_DICT = {
    1: "internal",
    2: "external"
}

print("Connected to micro:bit at " + mb_node)

credentials = get_credentials()
client = InfluxDBClient(
    credentials['host'],
    credentials['port'],
    credentials['username'],
    credentials['password'])

client.switch_database(credentials['database'])

print("Connected to InfluxDB at " + credentials['host'])
print("Waiting for incoming data...")

# Loop forever listening for data.
while True:
    try:
        # Read the data.
        all_data = ser.readline().strip().decode('utf-8')

        if all_data != "":
            # Split into an array of comma-separated values.
            data = all_data.split(",")

            key = data[0]
            ident = int(data[1])
            temp = data[2]

            if key != AUTH_KEY:
                print("Data output with incorrect authentication key? " + all_data)
                continue

            name = ID_LOCATION_DICT[ident]
            temp = int(temp)

            send_data_to_influx(name, temp)
    except Exception as e:
        # Pass all exceptions except for KeyboardInterrupt (exiting the program)
        if isinstance(e, KeyboardInterrupt):
            raise
    
        raise
