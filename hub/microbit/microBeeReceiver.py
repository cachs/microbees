# microBeeReceiever.py
# Receives incoming temperature data from radio and outputs via serial.
# Inspired by Giles Booth's 'beehiveRX.py'
# CardyHoney, 2021 (Jacob Passam)

from microbit import *
import radio

radio.config(group=1)
radio.on()

display.show(Image.HEART)
sleep(2500)
display.clear()

def show():
    display.scroll('x')

while True:
    message = radio.receive()

    if message:
        print(message)
        show()
