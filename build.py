# build.py
# Builds Python scripts programatically into .hex micro:bit files.
# Requires pip module 'uflash' (https://pypi.org/project/uflash/)
# CardyHoney, 2021 (Jacob Passam)

import os

files = [
    ['../monitor/microBeeMonitor.py', 'microBeeMonitor.hex'],
    ['../hub/microbit/microBeeReceiver.py', 'microBeeReceiver.hex']
]

try:
    os.mkdir('build')
except FileExistsError:
    pass

os.chdir('build')

for file in files:
    path = file[0]
    name = file[1]
    
    os.system('uflash ' + path + ' .')
    os.rename('micropython.hex', name)

    print('Successfully created ' + name + '.')
    print()
