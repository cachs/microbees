# microBeeMonitor.py
# Monitors temperature data every 5 seconds and outputs over radio.
# Inspired by Giles Booth's 'beehiveTX.py'
# CardyHoney, 2021 (Jacob Passam)

from microbit import display, temperature, sleep
import radio

# Reads and returns a file's contents. If it doesn't exist, it is created with
# the default specified.
def read_or_create_file(name, default):
    try:
        with open(name) as f:
            return f.read()
    except OSError:
        # File not exists

        with open(name, "w") as opened:
            opened.write(str(default))

        return str(default)


DEVICE_ID = read_or_create_file("device_id.dat", 1)
AUTH_KEY = read_or_create_file("auth_key.dat", "YourAuthenticationKey")

radio.config(group=1)

while True:
    temp = temperature()

    # Enable/disable radio to save battery.
    radio.on()
    radio.send(AUTH_KEY + "," + DEVICE_ID + "," + str(temp))
    radio.off()

    sleep(5000)
