![](https://www.cardyhoney.com/assets/logo-small.svg)

# MicroBees

MicroBees is an in-house software suite used for CardyHoney's beehive monitoring systems. We currently deeply monitor temperature and provide a live camera feed of our hive!

This project is open-source for the benefit of our partner schools to create their own, similar, monitoring systems! All code is documented and tutorials/explanations are provided on how we run this project.

* [https://cardyhoney.com](https://cardyhoney.com)
* [https://cardinalallen.co.uk](https://cardinalallen.co.uk)

### Documentation
* [Our Tutorial](doc/our_tutorial.md) provides information on how to create your own hive monitoring system using our software.
* [Processing](doc/processing.md) documents the flow of data and how we handle incoming temperature data and camera feeds.
* [Cameras](doc/cameras.md) is a deeper dive into our solar-powered camera system.
* [Temperature](doc/temperature.md) is a deeper dive into a temperature system and how we process, and store, the data.
* [micro:bits](doc/microbits.md) shows how we utilise and communicate with BBC micro:bits within our project.
* [Grafana](doc/grafana.md) shows how we use Grafana for displaying data within CardyHoney.